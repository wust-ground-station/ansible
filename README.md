# Ansible

## Ping hosts

Install `poetry` and enter the python virtualenv. You can ping the hosts

```shell
$ poetry install --no-root
$ poetry shell
$ ssh-copy-id root@kalenica.local
(venv) $ ansible ground_stations -m ping -i inventory.yaml
```
